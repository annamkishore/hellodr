## React Native - POC, Backend: Google Firebase
---

1. Bitbucket URL
    - https://annamkishore@bitbucket.org/annamkishore/hellodr.git

## How to run

#### Pre-requisites
- NodeJS to be installed
- git to be installed
- After installing NodeJS
    - install expo cli
    - cmd> npm install expo-cli --global

#### Steps
1. Checkout the project, into some folder in your machine
    - cmd> git clone https://annamkishore@bitbucket.org/annamkishore/hellodr.git
2. Load dependencies
    - cmd> cd hellodr
    - cmd> npm install
3. Run the application
    - cmd> expo start
    - Now browser automatically opens, and you can see QR code
    - QR code can also seen in the console
4. Install Expo in Android Mobile, link below
    - https://play.google.com/store/apps/details?id=host.exp.exponent&hl=en_IN
5. Open Expo in Mobile
    - Scan the QR code which is displayed in Browser/Console
6. Application Launched
7. Done

#### Known issue
- A Yellow warning is shown for now, pls dismiss
