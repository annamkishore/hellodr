import React from 'react';
import AppNavigation from './app/app-navigation';

export default class App extends React.Component {
  render() {
    return (
      <AppNavigation />
    );
  }
}
