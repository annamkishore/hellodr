import {StyleSheet} from "react-native";

const BUTTON_COLOR = '#6558f5';

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',       // horizontal alignment
    // justifyContent: 'center',   // vertical alignment
  },
  welcomeMessage: {
    textAlign: 'center',
    textAlignVertical: 'center',
    marginTop:100,
    marginBottom:5,
    height:150,
    width:280,
    borderWidth: 1,
    borderColor: '#bdcbd5',
  },
  line: {
    height:1,
    width:285,
    borderBottomWidth: 1,
    borderColor: '#bdcbd5',
    marginBottom:80,
  },
  inputField: {
    width: 200,
    paddingTop: 5,
    paddingBottom: 5,
    paddingLeft: 10,
    paddingRight: 10,
    borderWidth: 1,
    borderColor: '#bdcbd5',
    color: '#8a98a2',
    marginBottom: 20,
    borderRadius:3,
  },
  loginButton: {
    width:70,
    height:35,
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom:20,
    borderRadius:3,
    backgroundColor: BUTTON_COLOR,
  },
  loginText: {
    color: 'white',
  },
  activity: {
    height: 50,
    marginTop:70,
  },
  buttonContainerLanding: {
    justifyContent: 'center',
    alignItems: 'center',
    width:140,
    height:35,
    marginTop:60,
    borderRadius:3,
    borderWidth: 1,
    borderColor: BUTTON_COLOR,
    backgroundColor: "#ffffff",
  },
  loginTextLanding: {
    color: BUTTON_COLOR,
  },
  pickerField: {
    width: 200,
    paddingTop: -5,
    paddingBottom: 0,
    paddingLeft: 0,
    paddingRight: 0,
    borderColor: '#bdcbd5',
    borderWidth: 1,
    borderRadius:3,
    color: '#8a98a2',
    marginBottom: 20,
  },
  head: { height: 40 },
  headText: { marginLeft: 10, marginRight: 10, marginTop: 6, marginBottom: 6, fontWeight: 'bold' },
  text: { marginLeft: 10, marginRight: 10, marginTop: 6, marginBottom: 6 }
});
