import React from 'react';
import { View, Picker, BackHandler } from 'react-native';

import { Table, Row, Rows } from 'react-native-table-component';

import ApiClient from './api/ApiClient';
import { doctorData } from "./mock-data/doctor-data";
import { styles } from "./styles";

export default class ListDoctors extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      specialities: null,
      specialityId: null,
      doctors: null,
      tableHead: ['Name', 'Speciality'],
    };
  }

  componentDidMount() {
    this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
      this.props.navigation.goBack();
      return true;
    });

    const specialities = [{id: "selectOne", name: "Select one"}, ...ApiClient.getSpecialities()];
    this.setState({specialities, specialityId: "selectOne"});
  }

  componentWillUnmount() {
    this.backHandler.remove();
  }


  render() {
    const state = this.state;
    const specialtyId = state.specialityId;
    let doctorsOfSpeciality = null;
    if(specialtyId && specialtyId !== "selectOne") {
      const specialityItem = doctorData.specialty.find(specialty => specialty.id === specialtyId);
      let doctorsIdList = specialityItem.doctors;
      doctorsOfSpeciality = doctorsIdList.map(
            doctorId => [doctorData.doctor.find(doctor => doctor.id === doctorId).name, specialityItem.name]);
    }

    return (
      <View style={styles.container}>
        <View style={[styles.pickerField, {marginTop: 100}]}>
          <Picker
            selectedValue={state.specialityId}
            onValueChange={(itemValue) => this.setState({specialityId: itemValue})}>
            {
              state.specialities &&
                state.specialities.map(
                  speciality => <Picker.Item key={speciality.id} label={speciality.name} value={speciality.id} />)
            }
          </Picker>
        </View>

        {
          doctorsOfSpeciality &&
          <View style={{width: 250}}>
            <Table borderStyle={{borderWidth: 1, borderColor: '#bdcbd5'}}>
              <Row data={state.tableHead} widthArr={[100, 150]} style={styles.head} textStyle={styles.headText}/>
              <Rows data={doctorsOfSpeciality} widthArr={[100, 150]} textStyle={styles.text}/>
            </Table>
          </View>
        }
      </View>
    );
  }
}
