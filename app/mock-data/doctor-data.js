export const doctorData = {
  welcomeMessage: "Welcome to the app",
  users: [
    {
      username: "annamkishore@gmail.com",
      password: "password123"
    },
    {
      username: "chandrau@gmail.com",
      password: "password123"
    }
  ],
  specialty: [
    {
      id: "s1",
      name: "Internal Medicine",
      doctors: ["d1"],
    },
    {
      id: "s2",
      name: "Dermatology",
      doctors: ["d2"],
    },
    {
      id: "s3",
      name: "Cardiology",
      doctors: ["d3"],
    },
    {
      id: "s4",
      name: "Nurology",
      doctors: ["d4", "d5"],
    },
    {
      id: "s5",
      name: "Ancology",
      doctors: ["d5"],
    },
  ],
  doctor: [
    {
      id: "d1",
      name: "Dr.Foo"
    },
    {
      id: "d2",
      name: "Dr.Ram"
    },
    {
      id: "d3",
      name: "Dr.Gokale"
    },
    {
      id: "d4",
      name: "Dr.Krishna"
    },
    {
      id: "d5",
      name: "Dr.Vijay"
    }
  ]
};
