import React from 'react';
import { Text, TouchableHighlight, ActivityIndicator, View } from 'react-native';

import { styles } from './styles'

import ApiClient from './api/ApiClient';

export default class Landing extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      welcomeMessage: "loading..",
      isLoading: true,
    }
  }

  async componentDidMount() {
    await ApiClient.loadData();

    let welcomeMessage = ApiClient.getWelcomeMessage();
    this.setState({welcomeMessage, isLoading: false});
  }

  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.welcomeMessage}>
          {this.state.welcomeMessage}
        </Text>
        <Text style={styles.line} />

        <View style={styles.activity}>
          {
            // this.state.isLoading && <ActivityIndicator size={75} color="#bdcbd5"/>
          }
        </View>


        <TouchableHighlight
          style={[styles.buttonContainerLanding]}
          disabled={this.state.isLoading}
          onPress={() => {this.props.navigation.push('LoginPage');}}>
          <Text style={styles.loginTextLanding}>Login to my app</Text>
        </TouchableHighlight>
      </View>
    );
  }
}
