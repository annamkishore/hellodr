import {createStackNavigator, createAppContainer} from 'react-navigation';

import Login from './login'
import Landing from './landing'
import ListDoctors from './list-doctors'

const navigationRules = {
  LandingPage: {screen: Landing},
  LoginPage: {screen: Login},
  ListDoctorsPage: {screen: ListDoctors},
};

const AppNavigator = createStackNavigator(
  navigationRules,
  {
    initialRouteName: "LandingPage",
    headerMode: 'none',
    navigationOptions: {
      headerVisible: false,
    },
  }
);

export default createAppContainer(AppNavigator);
