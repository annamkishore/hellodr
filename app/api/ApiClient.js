import { doctorData } from '../mock-data/doctor-data';
import { authenticateUsingFirebase, loadDataFromFirebase } from './FirebaseApi';

class ApiClient {
  constructor() {
    this.data = null;
    // this.data = doctorData;  // for mock testing
  }

  authenticate(username, password) {
    return authenticateUsingFirebase(username, password);
  }

  async loadData() {
    this.data = this.data || await loadDataFromFirebase();
  }

  getWelcomeMessage() {
    return this.data.welcomeMessage;
  }

  getSpecialities() {
    return this.data.specialty;
  }

  getDoctors(specialityId) {
    let specialityDoctors = this.data.specialty.find(item => item.id === specialityId && item.doctors);
    return specialityDoctors.map(item => this.data.doctor.find(item));
  }

  //----------------mock methods from here
  authenticate_usingMock(username, password) {
    return doctorData.users.some(item => item.username === username && item.password === password);
  }
}

export default new ApiClient();
