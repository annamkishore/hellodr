import firebase from 'firebase';

export function authenticateUsingFirebase(email, password) {
  return firebase.auth().signInWithEmailAndPassword(email, password);
}

export async function loadDataFromFirebase() {
  let snapshot = await firebase.database().ref().once('value');
  let data = snapshot.val();
  console.log(data);
  return data;
}

//  Initialize Firebase
(function initializeFirebase() {
  if(firebase.apps.length) {
    return;
  }

  var config = {
    apiKey: "AIzaSyAmCsXd_NbJvlsQMvr3dw1ILTwT9l27dPw",
    authDomain: "hellodr-poc.firebaseapp.com",
    databaseURL: "https://hellodr-poc.firebaseio.com",
    storageBucket: "hellodr-poc.appspot.com",
  };
  firebase.initializeApp(config);
})(); // self calling function
