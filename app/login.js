import React from 'react';
import { BackHandler, Text, TouchableHighlight, TextInput, View } from 'react-native';

import { styles } from './styles'

import ApiClient from './api/ApiClient';

export default class Login extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      username: null,
      password: null,
      isLoginSuccess: null,
      disableLogin: false,
    };

    this.login = this.login.bind(this);
  }

  componentDidMount() {
    this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
      this.props.navigation.goBack();
      return true;
    });
  }

  componentWillUnmount() {
    this.backHandler.remove();
  }

  async login() {
    this.setState({disableLogin: true});
    const {username, password} = this.state;

    // let success = ApiClient.authenticate_usingMock(username, password);
    try {
      let userObject = await ApiClient.authenticate(username, password);  // not using userObject
      this.setState({isLoginSuccess: true});
      this.gotoDoctorsPage();
    }catch(err) {
      this.setState({isLoginSuccess: false});
    }
    this.setState({disableLogin: false});
  }

  gotoDoctorsPage() {
    this.props.navigation.push('ListDoctorsPage');
  }

  render() {
    return (
      <View style={styles.container}>
        <TextInput
          placeholder='Login/email'
          value={this.state.username}
          onChangeText={(username) => this.setState({ username })}
          style={[styles.inputField, {marginTop: 100}]}
        />
        <TextInput
          placeholder='Password'
          secureTextEntry={true}
          value={this.state.password}
          onChangeText={(password) => this.setState({ password })}
          style={styles.inputField}
        />
        <TouchableHighlight
          style={[styles.loginButton]}
          onPress={this.login}
          disabled={this.state.disableLogin}>
          <Text style={styles.loginText}>Login</Text>
        </TouchableHighlight>
        {
          this.state.isLoginSuccess === false &&
            <Text>
              Invalid Credentials!!
            </Text>
        }
      </View>
    );
  }
}
