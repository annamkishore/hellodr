

---Below: known issues----------------
1)
thickness of throbber

2)
combobox uparrow missing

3)
combobox showing as popup instead of expanding there

4)
warning, yellow box

---Below: Questions----------------
1)
what to do when Back button clicked in each of the screen

2)
What if the table grows bigger

3)
Authentication, shall add couple of users for authentication
will simply show 2 seconds for now..
may be I shall get clue while integrating firebase backend

4)
What data to load in the landing screen, i.e even before login

---Below: Development pipeline---------------

1)
todo: frontend validations

2)
todo: generate apk

---Below: Development activity----------------

Day #3) 8 hours
November 24th 2018, Saturday
    firebase authentication (using email/password): 3 hours
    firebase Databases: 3 hours
    ApiClient with firebase: 1 hour
    Readme.md - how to run documentation: 1 hour

Day #2) 10.5 hours
November 23rd 2018, Friday
    bitbucket setup: 1 hour
    react navigation: 2 hours
    back button handler, navigation header removal: 1 hour
    table ui: 1 hour
    mock data, ApiClient using mock, ApiClient using in all 3 screens: 1+1+2 = 4 hours
    CSS, over all application look: 1.5 hours

Day #1) 6 hours
November 22nd 2018, Thursday
    Due to firewall issue, effort wasted 2 hours, for expo created basic app to run: 2 hours
    skeleton development: 4 hours

---Below: Git related------------
git clone https://annamkishore@bitbucket.org/annamkishore/hellodr.git

set git remote to ==> https://annamkishore@bitbucket.org/annamkishore/hellodr.git
Then, perform
git fetch
git branch --set-upstream-to origin/master
git pull origin master --allow-unrelated-histories
git push

---Below: create and run react native app------------

Create React Native Project - 5 mins
    https://facebook.github.io/react-native/
    click on Get Started
    https://facebook.github.io/react-native/docs/getting-started
    thats all

Disabled - Network Connections
    VirtualBox Host only network, as first local IP is being used for Expo while starting server
    windows firewall
        todo: to add inbound rule, instead of disabling the firewall

----Below: FYI--------------

Ports - FYI
    http://localhost:19002/
        expo dev tools
    metro bundler - port: 19001
    http://192.168.1.5:19000/
        json output

Not working in bash - todo, why not working
    cmd> expo init ProjectName
    not working in bash, only working in windows cmd

-----Below: wrong info in google, may be new version of expo doesn't need this-----------------

env variables - NOT REQUIRED
    REACT_NATIVE_PACKAGER_HOSTNAME
    192.168.1.5
